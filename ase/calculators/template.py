import os
import re

import numpy as np
import ase.io.xyz
from ase.units import Hartree, Bohr
from ase.calculators.calculator import FileIOCalculator, Parameters, ReadError

class template(FileIOCalculator):
 implemented_properties = ['energy', 'forces']
 command = 'bash run_template.sh PREFIX'

 def __init__(self, restart=None, ignore_bad_restart_file=False, label='geom', atoms=None, **kwargs):
  FileIOCalculator.__init__(self, restart, ignore_bad_restart_file, label, atoms, **kwargs)

 def set(self, **kwargs):
  changed_parameters = FileIOCalculator.set(self, **kwargs)
  if changed_parameters:
   self.reset()

 def write_input(self, atoms, properties=None, system_changes=None):
  FileIOCalculator.write_input(self, atoms, properties, system_changes)

  cmt="energy"
  if "forces" in properties:
   cmt="gradient"
    
  with open(self.label+".xyz","w") as fd:
   ase.io.xyz.write_xyz(fd,self.atoms,comment=cmt)

 def read(self, label):
  FileIOCalculator.read(self, label)
  if not os.path.isfile("run_template.sh"):
   raise ReadError

  self.read_results()

 def read_results(self):
  if not os.path.isfile(self.label + '.energy'):
   raise ReadError
  self.read_energy()
  grad=False
  with open(self.label+".xyz","r") as f:
   line=f.readline()
   line=f.readline()
   if "gradient" in line:
    grad=True
  if grad:
   if not os.path.isfile(self.label + '.grad'):
    raise ReadError
   self.read_forces()

 def read_energy(self):
  self.results['energy']=np.loadtxt(f'{self.label}'+".energy")

 def read_forces(self):
  gradients=np.loadtxt(f'{self.label}'+".grad")
  self.results['forces'] = - gradients * Hartree / Bohr
